FROM debian:buster-slim

# install dependencies
RUN set -eux; \
    apt-get update \
    && apt-get install -y \
        apt-transport-https \
        build-essential \
        ca-certificates \
        curl \
        git \
        gnupg \
        iputils-ping \
        lsb-release \
        netbase \
        software-properties-common \
        ssh-client \
        unzip \
        wget \
    && rm -rf /var/lib/apt/lists/*

# install dotnet
RUN wget https://packages.microsoft.com/config/debian/10/packages-microsoft-prod.deb -O packages-microsoft-prod.deb \
        && dpkg -i packages-microsoft-prod.deb \
        && rm packages-microsoft-prod.deb \
        && apt-get update \
        && apt-get install -y dotnet-sdk-6.0

# install doctl
RUN curl -sSL https://raw.githubusercontent.com/fonixtelematics/pipelines/main/get-doctl.sh | sh

# install yq
RUN curl -sSL https://raw.githubusercontent.com/fonixtelematics/pipelines/main/get-yq.sh | sh

# install kubectl
RUN apt-get update \
   && curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg \
   && echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | tee /etc/apt/sources.list.d/kubernetes.list \
   && apt-get update \
   && apt-get install -y kubectl

# install faas-cli
RUN curl -sSL https://cli.openfaas.com | sh

# Create dirs and users
RUN mkdir -p /opt/ci/build \
    && sed -i '/[ -z \"PS1\" ] && return/a\\ncase $- in\n*i*) ;;\n*) return;;\nesac' /root/.bashrc \
    && useradd --create-home --shell /bin/bash --uid 1000 pipelines

WORKDIR /opt/ci/build

ENTRYPOINT /bin/bash
