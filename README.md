# Bitbucket Pipelines for DigitalOcean deployments

This project provides a Bitbucket Pipelines build environment that deploys into DigitalOcean.

The image is based on Atlassian's default build image and includes the latest version of `doctl`.

# Usage

Just use [fonixtelematics/pipeline-digitalocean:2](https://hub.docker.com/r/fonixtelematics/pipeline-digitalocean) as the build image, instead of the default one.

You'll need to provide an API access token in order to use `doctl` on the scripts. Create a repository (or account) environment variable and store the token securely. You'll be able to reference it on the scripts.

Here's an example to authenticate the API access token, followed by a login on the Container Registry (to push images).

```yaml
script:
  - ...
  - doctl auth init --access-token $DOCTL_API_KEY
  - doctl registry login
```

# Sample Pipeline

The file [pipeline-template.yml](pipeline-template.yml) contains a pipeline example using Docker and `doctl`.
